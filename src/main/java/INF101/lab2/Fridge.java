package INF101.lab2;


import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    int max_size = 20;
    
    ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();
    
    public int totalSize() {   
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        int fridgeItems = items.size();
        return fridgeItems;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (items.size() < max_size) {
            return items.add(item);
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!items.contains(item)) {
            throw new NoSuchElementException();
        }
        items.remove(item);
        
    }

    @Override
    public void emptyFridge() {
        items.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expItems = new ArrayList<FridgeItem>();
        for(FridgeItem item : items) {
            if(item.hasExpired()) {
                expItems.add(item);
            }
        }
        items.removeAll(expItems);
        return expItems;
    }
}
